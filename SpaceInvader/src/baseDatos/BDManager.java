package baseDatos;

import java.sql.*;

public class BDManager {
	Connection conexion = null;

	public Connection open() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			try {
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbspaceinvader", "root", "");
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("No se pudo establecer la conexion");
			}
		} catch (ClassNotFoundException e) {
			System.out.println("Driver no encontrado");
		}
		return conexion;
	}

	public void close() {
		try {
			conexion.close();
		} catch (SQLException e) {
			System.out.println("No se pudo cerrar la conexion");
		}
	}
}
