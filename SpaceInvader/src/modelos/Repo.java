package modelos;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import baseDatos.BDManager;

public class Repo {
	private static BDManager database = new BDManager();

	public static void crear(Usuario usuario) {
		try {
			String query = "INSERT INTO usuario (Id,Nombre,Apellido,Edad,Identidad,FechaNacimiento,FechaRegistro,Foto,Nickname,Password) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			PreparedStatement stmt = database.open().prepareStatement(query);
			stmt.setInt(1, 0);
			stmt.setString(2, usuario.getNombre());
			stmt.setString(3, usuario.getApellido());
			stmt.setString(4, usuario.getEdad());
			stmt.setString(5, usuario.getIdentidad());
			stmt.setString(6, usuario.getFechaNacimiento());
			stmt.setString(7, usuario.getFechaRegistro());
			stmt.setString(8, usuario.getFoto());
			stmt.setString(9, usuario.getNickname());
			stmt.setString(10, usuario.getPassword());
			stmt.executeUpdate();
			stmt.close();
			database.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("mal");
		}
	}
}
