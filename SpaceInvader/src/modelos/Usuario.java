package modelos;

public class Usuario {
	private int id = 0;
	private String nombre;
	private String apellido;
	private String edad;
	private String identidad;
	private String fechaNacimiento;
	private String fechaRegistro;
	private String foto;
	private String nickname;
	private String password;
	
	public Usuario(int id, String nombre, String apellido, String edad, String identidad, String fechaNacimiento,
			String fechaRegistro, String foto, String nickname, String password) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.identidad = identidad;
		this.fechaNacimiento = fechaNacimiento;
		this.fechaRegistro = fechaRegistro;
		this.foto = foto;
		this.nickname = nickname;
		this.password = password;
	}
	
	public static Usuario crear(int id, String nombre, String apellido, String edad, String identidad, String fechaNacimiento,
			String fechaRegistro, String foto, String nickname, String password) {
        return new Usuario(id, nombre, apellido, edad, identidad, fechaNacimiento,
    			fechaRegistro, foto, nickname, password);
    }
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEdad() {
		return edad;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public String getIdentidad() {
		return identidad;
	}
	public void setIdentidad(String identidad) {
		this.identidad = identidad;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
