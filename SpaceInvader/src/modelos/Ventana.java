package modelos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.sun.xml.internal.ws.api.Component;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;

import javafx.scene.control.PasswordField;

public class Ventana extends JFrame {
	private JPanel panel1, panel2;
	private JLabel imagen;
	private ImageIcon icono;
	private JComboBox menu;
	private String[] opciones = { "MENU", "REGISTRO", "LISTADO DE USUARIOS", "MEJORES PUNTAJES", "JUGAR" };

	public Ventana(String titulo) {
		super(titulo);
		this.iniciar();
		this.configurarComponentes();
		this.asignarEventos();

		this.pack();
		this.setVisible(true);
	}

	private Ventana iniciar() {
		Dimension dims = new Dimension(800, 700);
		this.setSize(dims);
		this.setPreferredSize(dims);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		return this;

	}

	private Ventana configurarComponentes() {
		// crear paneles
		panel1 = new JPanel(new GridBagLayout());
		panel2 = new JPanel(new GridBagLayout());
		panel1.setBorder(new TitledBorder("MENU"));
		panel2.setBorder(new TitledBorder("DESARROLLO"));
		panel1.setBackground(Color.BLACK);

		// componentes panel 1
		imagen = new JLabel();
		icono = new ImageIcon("img/SpaceInvader.jpg");
		imagen.setIcon(icono);
		imagen.setHorizontalAlignment(JLabel.CENTER);
		add(imagen, BorderLayout.CENTER);
		menu = new JComboBox(opciones);

		GridBagConstraints gbc = new GridBagConstraints();
		Insets insets = new Insets(0, 200, 0, 200);
		gbc.gridwidth = 1; // cuantas columnas coge
		gbc.gridheight = 1; // cuantas filas coge
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		gbc.fill = GridBagConstraints.BOTH;

		gbc.gridx = 0;
		gbc.gridy = 0;
		panel1.add(imagen, gbc);

		gbc.insets = insets;
		gbc.gridx = 0;
		gbc.gridy = 1;
		panel1.add(menu, gbc);

		// a�adir paneles al JFrame
		this.add(panel1, BorderLayout.NORTH);
		this.add(panel2, BorderLayout.CENTER);
		return this;
	}

	private void asignarEventos() {
		EvtMenu manejador = new EvtMenu();

		menu.addActionListener(manejador);

	}

	private class EvtMenu implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (menu.getSelectedItem() == "REGISTRO") {
				new Registro();
			}
			if (menu.getSelectedItem() == "LISTADO DE USUARIOS") {

			}
			if (menu.getSelectedItem() == "MEJORES PUNTAJES") {

			}
			if (menu.getSelectedItem() == "JUGAR") {
				
			}
		}

	}

	private class Registro {
		JButton registrar, avatar, foto;
		JLabel nombreL, apellidoL, edadL, identidadL, fNacimientoL, nicknameL, passwordL;
		ImageIcon imagen;
		JTextField nombreBox, apellidoBox, edadBox, identidadBox, nicknameBox;
		JPasswordField passwordBox;
		LocalDate fRegistro;
		JDateChooser fNacimiento;
		String imagenUsuario;
		Usuario usuario;

		public Registro() {
			Ventana.this.panel2.removeAll();
			Ventana.this.panel2.revalidate();
			Ventana.this.panel2.repaint();
			this.componentes();
			this.eventos();
		}

		private void componentes() {
			nombreL = new JLabel("NOMBRE:");
			apellidoL = new JLabel("APELLIDO:");
			edadL = new JLabel("EDAD:");
			identidadL = new JLabel("NUMERO DE IDENTIDAD:");
			fNacimientoL = new JLabel("FECHA DE NACIMIENTO:");
			nicknameL = new JLabel("NICKNAME:");
			passwordL = new JLabel("PASSWORD:");

			nombreBox = new JTextField(10);
			apellidoBox = new JTextField(10);
			edadBox = new JTextField(10);
			identidadBox = new JTextField(10);
			nicknameBox = new JTextField(10);

			registrar = new JButton("REGISTRAR");
			avatar = new JButton("ESCOJER IMAGEN");
			foto = new JButton();

			passwordBox = new JPasswordField();

			fNacimiento = new JDateChooser();

			GridBagConstraints gbc = new GridBagConstraints();
			Insets insets = new Insets(5, 20, 5, 20);
			gbc.insets = insets;
			gbc.gridwidth = 1; // cuantas columnas coge
			gbc.gridheight = 1; // cuantas filas coge
			gbc.weightx = 1.0;
			gbc.weighty = 1.0;
			gbc.fill = GridBagConstraints.BOTH;

			gbc.gridx = 0;
			gbc.gridy = 0;
			panel2.add(nombreL, gbc);
			gbc.gridx = 1;
			gbc.gridy = 0;
			panel2.add(nombreBox, gbc);

			gbc.gridx = 0;
			gbc.gridy = 1;
			panel2.add(apellidoL, gbc);
			gbc.gridx = 1;
			gbc.gridy = 1;
			panel2.add(apellidoBox, gbc);

			gbc.gridx = 0;
			gbc.gridy = 2;
			panel2.add(edadL, gbc);
			gbc.gridx = 1;
			gbc.gridy = 2;
			panel2.add(edadBox, gbc);

			gbc.gridx = 0;
			gbc.gridy = 3;
			panel2.add(identidadL, gbc);
			gbc.gridx = 1;
			gbc.gridy = 3;
			panel2.add(identidadBox, gbc);

			gbc.gridx = 0;
			gbc.gridy = 4;
			panel2.add(fNacimientoL, gbc);
			gbc.gridx = 1;
			gbc.gridy = 4;
			panel2.add(fNacimiento, gbc);

			gbc.gridx = 0;
			gbc.gridy = 5;
			panel2.add(nicknameL, gbc);
			gbc.gridx = 1;
			gbc.gridy = 5;
			panel2.add(nicknameBox, gbc);

			gbc.gridx = 0;
			gbc.gridy = 6;
			panel2.add(passwordL, gbc);
			gbc.gridx = 1;
			gbc.gridy = 6;
			panel2.add(passwordBox, gbc);

			gbc.gridx = 2;
			gbc.gridy = 4;
			panel2.add(avatar, gbc);

			gbc.gridx = 2;
			gbc.gridy = 7;
			panel2.add(registrar, gbc);

			gbc.gridwidth = 1; // cuantas columnas coge
			gbc.gridheight = 4; // cuantas filas coge
			gbc.gridx = 2;
			gbc.gridy = 0;
			panel2.add(foto, gbc);

		}

		private void eventos() {
			EvtRegistro manejador = new EvtRegistro();

			nombreBox.addKeyListener(manejador);
			apellidoBox.addKeyListener(manejador);
			edadBox.addKeyListener(manejador);
			identidadBox.addKeyListener(manejador);

			avatar.addMouseListener(manejador);
			registrar.addMouseListener(manejador);
		}

		private class EvtRegistro implements KeyListener, MouseListener {

			@Override
			public void keyTyped(KeyEvent e) {

				if (e.getComponent() == nombreBox) {
					if ((e.getKeyChar() != ' ' && (e.getKeyChar() < 'a' || e.getKeyChar() > 'z'))
							|| nombreBox.getText().length() > 20) {
						e.consume();
					}
				}

				if (e.getComponent() == apellidoBox) {
					if ((e.getKeyChar() != ' ' && (e.getKeyChar() < 'a' || e.getKeyChar() > 'z'))
							|| apellidoBox.getText().length() > 20) {
						e.consume();
					}
				}

				if (e.getComponent() == edadBox) {
					if (e.getKeyChar() < '0' || e.getKeyChar() > '9' || edadBox.getText().length() > 1) {
						e.consume();
					}
				}

				if (e.getComponent() == identidadBox) {
					if (e.getKeyChar() < '0' || e.getKeyChar() > '9' || identidadBox.getText().length() > 9) {
						e.consume();
					}
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {

				if (e.getComponent() == avatar) {
					JFileChooser chooser = new JFileChooser();
					FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG & PNG Images", "jpg", "png");
					chooser.setFileFilter(filter);
					int returnVal = chooser.showOpenDialog(getParent());
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						imagenUsuario = chooser.getSelectedFile().getPath();
						imagen = new ImageIcon(chooser.getSelectedFile().getPath());
						icono = new ImageIcon(imagen.getImage().getScaledInstance(foto.getWidth(), foto.getHeight(),
								Image.SCALE_DEFAULT));
						foto.setIcon(icono);
					}
				}

				if (e.getComponent() == registrar && nombreBox.getText().trim().length() != 0
						&& apellidoBox.getText().trim().length() != 0 && edadBox.getText().trim().length() != 0
						&& identidadBox.getText().trim().length() != 0 && fNacimiento.getDate() != null
						&& nicknameBox.getText().trim().length() != 0 && passwordBox.toString().trim().length() != 0
						&& imagenUsuario != null) {
					fRegistro = LocalDate.now();
					usuario = Usuario.crear(0, nombreBox.getText(), apellidoBox.getText(), edadBox.getText(),
							identidadBox.getText(), fNacimiento.getDate().toString(), fRegistro.toString(),
							imagenUsuario, nicknameBox.getText(), new String(passwordBox.getPassword()));
					Repo.crear(usuario);
					Ventana.this.panel2.removeAll();
					Ventana.this.panel2.revalidate();
					Ventana.this.panel2.repaint();
					JOptionPane.showMessageDialog(panel2, "DATOS INGRESADOS CON EXITO", "Mensaje del Sistema",
							JOptionPane.INFORMATION_MESSAGE);
				} else if (e.getComponent() == registrar) {
					JOptionPane.showMessageDialog(panel2, "POR FAVOR LLENE TODOS LOS CAMPOS", "Mensaje del Sistema",
							JOptionPane.ERROR_MESSAGE);
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
			}

		}
	}
}
